 class JokeApi{

    _BASE_API_URL = "https://icanhazdadjoke.com/";

    async getData(url = ""){
        const fullURL = this._BASE_API_URL + url;
        const res = await fetch(fullURL, {headers:{"Accept":"application/json"}});
        if(!res.ok){
            throw new Error(`Could not fetch ${fullURL}`);
        }
        const body = await res.json();
        return body;
    }

    getRandomJoke(){
        return this.getData();
    }

    async getJokeByTerm(term, page = 1){
        const result = await this.getData(`search?term=${term}&page=${page}&limit=10`);
        return result;
    }
}
export default  new JokeApi();