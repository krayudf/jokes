const initialState = {
  randomJoke: undefined,
  searchJokesResult: {},
  bestJokes: []
};

const reducer = (state = initialState, action) => {

    switch(action.type) {
        case "RANDOM_JOKE_LOADED":
            return {
                ...state,
                randomJoke: action.payload
            };
        case "INSERT_JOKE_TO_FAVORITE":
            const newBestJokesArray = [...state.bestJokes, action.payload];
            localStorage.setItem("bestJokes", JSON.stringify(newBestJokesArray));
            return{
                ...state,
                bestJokes: newBestJokesArray
            };
        case "REMOVE_JOKE_FROM_FAVORITE":
        {
            let newBestJokesArray = [];
            state.bestJokes.forEach((joke, index) => {
                if(joke.id === action.payload)
                {
                    newBestJokesArray = [...state.bestJokes.slice(0, index), ...state.bestJokes.slice(index + 1)];
                    return;
                }
            });
            localStorage.setItem("bestJokes", JSON.stringify(newBestJokesArray));
            
            return {
                ...state,
                bestJokes: newBestJokesArray
            };
        }
        case "FOUNDED_JOKES":
            return{
                ...state,
                searchJokesResult: action.payload
            };
        
        case "LOADED_BEST_JOKES_FROM_LOCAL_STORAGE":
            return {
                ...state,
                bestJokes: action.payload
            };
        
        default:
            return state;
    }
};

export default reducer;