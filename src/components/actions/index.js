const jokeLoaded = (newJoke) => {
    return {
        type: "RANDOM_JOKE_LOADED",
        payload: newJoke
    };
};

const addJokeToFavorite = (joke) => {
    return {
        type: "INSERT_JOKE_TO_FAVORITE",
        payload: joke
    };
};

const removeJokeFromFavorite = (id) => {
    return {
        type: "REMOVE_JOKE_FROM_FAVORITE",
        payload: id
    };
};

const foundedJokes = (searchJokesResult) => {
    return {
        type: "FOUNDED_JOKES",
        payload: searchJokesResult
    };
};

const loadedBestJokesFromLocalStorage = (bestJokes) => {
    return {
        type: "LOADED_BEST_JOKES_FROM_LOCAL_STORAGE",
        payload: bestJokes
    };
};


export {
    jokeLoaded,
    addJokeToFavorite,
    removeJokeFromFavorite,
    foundedJokes,
    loadedBestJokesFromLocalStorage
};