import React, {Component} from "react";
import { debounce } from "debounce";
import { connect } from "react-redux";

import JokeAPI from "../../services/jokeapi";
import Joke from "../joke/joke";

import "./search-joke.css";

class SearchJoke extends Component{
    constructor(){
        super();

        this.resolveRequest = true;
    }

    onInputChange = () => {
        const searchValue = document.getElementById("searchInput").value;
        this.searchJokeByTerm(searchValue);
    }

    searchJokeByTerm = (searchValue) =>{
        JokeAPI.getJokeByTerm(searchValue).then((res) => {
            this.props.foundedJokes(res);
        });
    }

    nextPage = () => {
        const {search_term, next_page} = this.props.searchJokesResult;
        JokeAPI.getJokeByTerm(search_term, next_page).then((res) => {
            this.props.foundedJokes(res);
        });
    }

    prevPage = () => {
        const {search_term, previous_page} = this.props.searchJokesResult;
        JokeAPI.getJokeByTerm(search_term, previous_page).then((res) => {
            this.props.foundedJokes(res);
        });
    }

    render(){
        const searchResult = this.props.searchJokesResult;
        let searchResulList;

        if(searchResult.results && searchResult.results.length > 0){
            searchResulList = searchResult.results.map(joke => {
                return(
                    <Joke key={joke.id} id={joke.id} joke={joke.joke} />
                )
            })
        }
        else{
            searchResulList = (<p>Результаты поиска отсутствуют</p>);
        }

        return(
           <div>
               <h3>Поиск шуток:</h3>
               <input id="searchInput" onChange={debounce(this.onInputChange, 500)} className="form-control form-control-lg search-joke-input" type="text" placeholder="Введите текст шутки..."></input>
                {searchResulList}

                { (searchResult.previous_page < searchResult.current_page)? <button onClick={this.prevPage} style={{marginRight:"10px"}} type="button" className="btn btn-primary btn-sm">Назад</button> : false}
                { (searchResult.next_page > searchResult.current_page) ? <button onClick={this.nextPage}  type="button" className="btn btn-primary btn-sm">Вперёд</button> : false}
           </div>
        );
    }
};

const mapStateToProps = ({searchJokesResult}) => {
    return {searchJokesResult};
};

const mapDispatchToProps = (dispatch) => {
    return {
        foundedJokes: (searchJokesResult) => {
            dispatch({
                type:"FOUNDED_JOKES",
                payload:searchJokesResult
            });
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchJoke);