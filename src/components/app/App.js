import React, { Component } from 'react';
import {BrowserRouter as Router, Route} from "react-router-dom";
import { connect } from "react-redux";

import './App.css';

import Navigation from "../nav/nav";
import RandomJoke from "../random-joke/random-joke";
import SearchJoke from "../search-joke/search-joke";
import BestJoke from "../best-joke/best-joke";

class App extends Component {

  componentDidMount(){
    const bestJokesFromLocalStorage = JSON.parse(localStorage.getItem("bestJokes"));
    if(bestJokesFromLocalStorage)
    {
      this.props.loadedBestJokesFromLocalStorage(bestJokesFromLocalStorage);
    }
  }

  render() {
    return (
          <section className="App">
            <Router>
                <div>
                  <Navigation/>
                  <section>
                        <Route path="/" exact component={RandomJoke}/>
                        <Route path="/search" component={SearchJoke}/>
                        <Route path="/best" component={BestJoke}/>
                  </section>
                </div>
            </Router>
          </section>
    );
  }
}

const mapStateToProps = ({searchJokesResult}) => {
  return {searchJokesResult};
};

const mapDispatchToProps = (dispatch) => {
  return {
      loadedBestJokesFromLocalStorage: (bestJokes) => {
          dispatch({
              type:"LOADED_BEST_JOKES_FROM_LOCAL_STORAGE",
              payload:bestJokes
          });
      }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
