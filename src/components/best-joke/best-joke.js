import React, {Component} from "react";
import { connect } from "react-redux";

import Joke from "../joke/joke";

class BestJoke extends Component{

    render(){
        const revertedArray = [...this.props.bestJokes].reverse();
        
        let bestJokesList;
        if(revertedArray.length > 0)
        {
            bestJokesList = revertedArray.map(joke => {
                return(
                    <Joke key={joke.id} id={joke.id} joke={joke.joke} />
                )
            })
        }
        else
        {
            bestJokesList = (<p>Лучших шуток пока нет :(</p>);
        }

        return(
            <div>
            <h3>Лучшие шутки:</h3>
            {bestJokesList}
            </div>
        );
    }
};


const mapStateToProps = ({bestJokes}) => {
    return {bestJokes};
};


export default connect(mapStateToProps)(BestJoke);