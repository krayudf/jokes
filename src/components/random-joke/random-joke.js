import React, {Component} from "react";
import { connect } from "react-redux";

import Joke from "../joke/joke";
import "./random-joke.css";


import JokeAPI from "../../services/jokeapi";

class RandomJoke extends Component{

    componentDidMount(){
        if(this.props.randomJoke === undefined) {
            this.getNewRandomJoke();
        }
    }

    getNewRandomJoke = () => {
        JokeAPI.getRandomJoke().then(res => {
            this.props.jokeLoaded(res);
        });
    };

    render(){
        const {randomJoke} = this.props;
        return(
            <div>
                <h3>Рандомная шутка:</h3>
                {(randomJoke) ? <Joke id={randomJoke.id} joke={randomJoke.joke}/> : false}
                <div className="random-joke__wrapper">
                    <button onClick={this.getNewRandomJoke} className="btn btn-primary btn-lg btn-block random-joke__more-btn">Ещё</button>
                </div>
            </div>
        );
    }
}

const mapStateToProps = ({randomJoke}) => {
    return {randomJoke};
};

const mapDispatchToProps = (dispatch) => {
    return {
        jokeLoaded: (newJoke) => {
            dispatch({
                type:"RANDOM_JOKE_LOADED",
                payload:newJoke
            });
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(RandomJoke);