import React from "react";
import {Link} from "react-router-dom";

const Navigation = () =>{
    return(
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">

            <div className="collapse navbar-collapse" id="navbarNav">
                <ul className="navbar-nav">
                    <li className="nav-item">
                        <Link className="nav-link" to="/">Случайная шутка</Link>
                    </li>
                    <li className="nav-item">
                        <Link className="nav-link" to="/search">Поиск шуток</Link>
                    </li>
                    <li className="nav-item">
                        <Link className="nav-link" to="/best">Лучшие шутки</Link>
                    </li>
                </ul>
            </div>
        </nav>
    );
};

export default Navigation;