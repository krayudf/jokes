import React, {Component} from "react";
import { connect } from "react-redux";

import "./joke.css";

class Joke extends Component {

    addToFavorite = () => {
        const {id, joke} = this.props;
        this.props.addJokeToFavorite({id, joke});
    };

    removeFromFavorite = () => {
        this.props.removeJokeFromFavorite(this.props.id);
    };


    render(){
        let isFavoriteJoke = false;

        this.props.bestJokes.forEach(bestJoke => {
            if(bestJoke.id === this.props.id){
                isFavoriteJoke = true;
            }
        });

        return(
             <div className="joke">
                 <div className="joke-text">{this.props.joke}</div>

                 <div className="joke-btn">
                     {(!isFavoriteJoke)
                         ? 
                         <button onClick={this.addToFavorite} type="button" className="btn btn btn-success">
                             <i className="fas fa-grin-alt"></i>
                         </button>
                         : 
                         <button onClick={this.removeFromFavorite} type="button" className="btn btn-danger">
                             <i className="far fa-trash-alt"></i>
                         </button>
                     }
                 </div>

                 <div className="clear"></div>
             </div>
        );
    };
}

const mapStateToProps = ({bestJokes}) => {
    return {bestJokes};
};

const mapDispatchToProps = (dispatch) => {
    return {
        addJokeToFavorite: (joke) => {
            dispatch({
                type:"INSERT_JOKE_TO_FAVORITE",
                payload:joke
            });
        },
        removeJokeFromFavorite: (id) => {
            dispatch({
                type:"REMOVE_JOKE_FROM_FAVORITE",
                payload:id
            });
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Joke);